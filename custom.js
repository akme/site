document.addEventListener('DOMContentLoaded', async () => {

  function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
      if (rawFile.readyState === 4 && rawFile.status == "200") {
        callback(rawFile.responseText);
      }
    }
    rawFile.send(null);
  }

  //articles:
  readTextFile("date.json", function (text) {
    var txt = JSON.parse(text);
    dates = txt['date']['txt'];
    var string_txt = '<ul>';
    for (let i = 0; i < dates.length; i++) {
      string_txt += '<li>';
      string_txt += '<span class="data">{' + dates[i]['date'];
      string_txt += ' ore ' + dates[i]['ora'] + '} </span>';
      string_txt += dates[i]['title'];
      var links = dates[i]['links'];
      
      if (links != "") {
        links.forEach((links) => {
          string_txt += '<span class="link"><a href="' + links[1] + '" target="_blank">'+links[0]+'&#8599;</a></span>';
      });
      }
      string_txt += '</li>';
    }
    string_txt += '</ul>';
    document.getElementById("articles").innerHTML = string_txt;
  });

  let location_url = window.location.href.split("#")[0];


  document.getElementById("txt").addEventListener('click', (e) => {
    e.preventDefault();
    document.getElementById("popup").classList.add("active");
    window.history.pushState(location_url, '', location_url + "#date");
  });

  var overlay = document.querySelectorAll('.overlay');

  overlay.forEach(element => {
    element.addEventListener("click", function (e) {
      e.preventDefault();
      document.querySelectorAll('.popup').forEach(el => el.classList.remove('active'));
      window.history.pushState(location_url, '', location_url);
    });
  });


  document.getElementById("close-popup-txt").addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelectorAll('.popup').forEach(el => el.classList.remove('active'));
    window.history.pushState(location_url, '', location_url);
  });

  if (window.location.hash === "#date") {
    document.getElementById("txt").click();
  }

});